Version {4.0.4}

Cara install:

<code>$ cd /var/www/html</code>

<code>$ git clone https://gitlab.com/eraporsmk/eraporsmk.git</code>

<code>$ git pull origin master</code>

<code>$ mv -v /eraporsmk/* /var/www/html</code>

<code>cd /var/www/html/application/config/</code>

<code>mv database.php.example database.php</code>

<code>$ nano database.php</code>

Edit bagian:

``` php?start_inline=1
'dsn'	=> 'pgsql:host=localhost;port=5432;dbname=your_database_name',
'hostname' => 'localhost:5432',
'username' => 'your_database_username',
'password' => 'your_database_password',
```
Sesuaikan nama database, port, username database dan password database.

Telah di uji coba di:

PHP versi 5.6

Apache versi 2.4