<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Auth_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation', 'curl'));
		$this->load->helper(array('url','language','sync'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}
	// redirect if needed, otherwise display the user list
	// create a new user
	public function index(){
		get_ta();
		$this->load->model('sekolah_model', 'sekolah');
		$sekolah = $this->sekolah->get_all();
		if($sekolah){
			redirect('admin/auth/');
		}
		$tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('npsn', 'NPSN', 'required');
        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');
		if ($this->form_validation->run() == true){
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');
			$npsn 		= $this->input->post('npsn');
			$data_sync = array(
				'username_dapo'	=> $email,
				'password_dapo'	=> $password,
				'npsn'			=> $npsn,
			);
			//test($data_sync);
			//die();
			$response = post_sync('register', $data_sync);
			//test($response);
			//die();
        }
        if ($this->form_validation->run() == true && $response->post_login){
			$gen_uuid = gen_uuid();
			$data_sekolah = array(
				//'sekolah_id'			=> $gen_uuid,
				'npsn' 					=> $response->data->npsn,
				'nss' 					=> ($response->data->nss) ? $response->data->nss : 0,
				'nama' 					=> $response->data->nama,
				'alamat' 				=> $response->data->alamat,
				'desa_kelurahan'		=> $response->data->desa_kelurahan,
				'kode_wilayah'			=> $response->data->kode_wilayah,
				'kecamatan' 			=> $response->data->kecamatan,
				'kabupaten' 			=> $response->data->kabupaten,
				'provinsi' 				=> $response->data->provinsi,
				'kode_pos' 				=> $response->data->kode_pos,
				'lintang' 				=> $response->data->lintang,
				'bujur' 				=> $response->data->bujur,
				'no_telp' 				=> $response->data->no_telp,
				'no_fax' 				=> $response->data->no_fax,
				'email' 				=> $response->data->email,
				'website' 				=> $response->data->website,
				//'logo_sekolah'			=> 0,
				//'guru_id'				=> 0,
				//'sekolah_id_dapodik'	=> $response->data->sekolah_id_dapodik,
				'sekolah_id'			=> $response->data->sekolah_id_dapodik,
				'created_at'			=> date('Y-m-d H:i:s'),
				'updated_at'			=> date('Y-m-d H:i:s'),
			);
			if($this->sekolah->insert($data_sekolah)){
				$additional_data = array(
					'user_id'		=> gen_uuid(),
					'npsn' 			=> $this->input->post('npsn'),
					'sekolah_id'  	=> $response->data->sekolah_id_dapodik,
					'password_dapo'	=> $response->data->password_dapo,
					'created_at'	=> date('Y-m-d H:i:s'),
					'updated_at'	=> date('Y-m-d H:i:s'),
				);
				//echo $identity;
				//test($data_sekolah);
				//test($additional_data);
				//die();
				$group = array('1');
				if($this->ion_auth->register('Administrator', $password, $email, $additional_data, $group)){
					$this->session->set_flashdata('success', $this->ion_auth->messages());
					redirect("admin/auth", 'refresh');
				} else {
					$this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					$this->template->title('eRapor SMK Bisa')
					->set_layout($this->auth_tpl)
					->set('page_title', 'Register')
					->build($this->auth_folder.'/register', $this->data);
				}
			} else {
				$this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
				$this->template->title('eRapor SMK Bisa')
				->set_layout($this->auth_tpl)
				->set('page_title', 'Register')
				->build($this->auth_folder.'/register', $this->data);
			}
            // check to see if we are creating the user
            // redirect them back to the admin page
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			if(isset($response)){
				if($response->post_login){
					$this->data['success'] = (validation_errors()) ? validation_errors() : $response->message;
				} else {
					$this->data['error'] = (validation_errors()) ? validation_errors() : $response->message;
				}
			} else {
				$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			}
            $this->data['npsn'] = array(
                'name'  => 'npsn',
                'id'    => 'npsn',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('npsn'),
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->template->title('eRapor SMK Bisa')
	        ->set_layout($this->auth_tpl)
	        ->set('page_title', 'Register')
	        ->build($this->auth_folder.'/register', $this->data);
        }
    }
}
