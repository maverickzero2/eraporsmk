<?php
class Proses_sync extends Backend_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('sync','file'));
		$this->load->library('curl');
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '-1');
	}
	public function index(){
		$settings = $this->settings->get(1);
		$user = $this->ion_auth->user()->row();
		$table_sync = array(
			1	=> 'absen',
			2	=> 'anggota_rombel',
			3	=> 'catatan_ppk',
			4	=> 'catatan_wali',
			5	=> 'deskripsi_mata_pelajaran',
			6	=> 'deskripsi_sikap',
			7	=> 'ekstrakurikuler',
			8	=> 'guru_terdaftar',
			9	=> 'jurusan_sp',
			10	=> 'kd_nilai',
			11	=> 'nilai',
			11	=> 'nilai_ekstrakurikuler',
			12	=> 'nilai_sikap',
			13	=> 'pembelajaran',
			14	=> 'prakerin',
			15	=> 'prestasi',
			16	=> 'ref_guru',
			17	=> 'ref_sekolah',
			18	=> 'ref_sikap',
			19	=> 'ref_siswa',
			20	=> 'remedial',
			21	=> 'rencana_penilaian',
			22	=> 'rombongan_belajar',
			23	=> 'teknik_penilaian',
		);
		$response = '';
		foreach($table_sync as $sync){
			$this->db->select('*');
			$this->db->from($sync);
			//$this->db->where("last_sync >= $settings->last_sync");
			$this->db->where('last_sync >=', $settings->last_sync);
			$query = $this->db->get();
			$result = $query->result();
			if($result){
				$kirim_data = kirim_data($user->sekolah_id, $sync, json_encode($result));
				//echo $kirim_data;
			}
			//echo $response;
		}
		$this->settings->update(1, array('last_sync' => date('Y-m-d H:i:s')));
	}
	public function test(){
		echo '5% [Mengirim data siswa]';
	}
}