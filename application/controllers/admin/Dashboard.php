<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends Backend_Controller {
	protected $activemenu = 'dashboard';
	public function __construct() {
		parent::__construct(); 
		$this->template->set('activemenu', $this->activemenu);
	}
	public function index(){
		//update_sync();
		$this->load->model('nilai_akhir_model','nilai_akhir');
		$loggeduser = $this->ion_auth->user()->row();
		//update_jurusan_sp($loggeduser->sekolah_id);
		//$sekolah = $this->sekolah->find_by_sekolah_id_dapodik($loggeduser->sekolah_id);
		$sekolah_id = $loggeduser->sekolah_id;
		$semester = get_ta();
		$jurusan = $this->jurusan_sp->get_all();
		if(!$jurusan){
			redirect('admin/sinkronisasi');
		}
		$data['detil_user']			= $this->ion_auth->user()->row();
		$data['sekolah_id'] 		= $sekolah_id;
		$data['semester'] 			= get_ta();
		$data['siswa'] 				= $this->anggota_rombel->find_count("sekolah_id = '$sekolah_id' AND semester_id = $semester->id");
		$data['guru'] 				= $this->guru_terdaftar->find_count("sekolah_id = '$sekolah_id' AND semester_id = $semester->id");
		$data['rombongan_belajar']	= $this->rombongan_belajar->find_count("sekolah_id = '$sekolah_id' AND semester_id = $semester->id");
		$data['rencana_penilaian']	= $this->rencana_penilaian->find_count("semester_id = $semester->id AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM rombongan_belajar WHERE sekolah_id = '$sekolah_id' AND semester_id = $semester->id)");
		//$data['nilai']				= $this->kd_nilai->find_count("rencana_penilaian_id IN(SELECT id FROM rencana_penilaian WHERE sekolah_id = $sekolah_id AND semester_id = $semester->id)");
		$data['nilai']				= $this->kd_nilai->find_count("rencana_penilaian_id IN(SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $semester->id)");
		//$this->nilai->find_count("semester_id = $semester->id");
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('page_title', 'Beranda')
		->build($this->admin_folder.'/dashboard', $data);
	}
	public function skin($id){
		$id = str_replace('_','-',$id);
		$this->load->library('user_agent');
		$newdata = array(
			'template'  => $id
				);
		$this->session->set_userdata($newdata);
		redirect($this->agent->referrer());
	}
	public function kunci_nilai($rombongan_belajar_id, $status){
		$text = ($status) ? 'menonaktifkan' : 'mengaktifkan';
		$update_rombel = $this->rombongan_belajar->update($rombongan_belajar_id, array('kunci_nilai' => $status));
		if($update_rombel){
			$this->session->set_flashdata('success', "Berhasil $text penilaian");
		} else {
			$this->session->set_flashdata('error', "Gagal $teks penilaian");
		}
		redirect('admin/dashboard#pembelajaran');
	}
}
