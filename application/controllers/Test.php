<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Test extends CI_Controller {
     
    public function __construct(){
		parent::__construct();
	} 
	//public function index(){
		//ini_set('post_max_size', '10M');
		//echo ini_get('post_max_size');
		//echo base64_encode(file_get_contents("./deskripsi_mata_pelajaran.json"));
	//}
	public function index(){
		$mata_pelajaran_id = 300210000;
		$rombongan_belajar_id = 2;
		$query = $this->db->query("
			SELECT 
				a.id as id_kd_nilai, 
				b.id as id_ref_kd, 
				c.id as id_rencana,
				a.kd_id as ref_kd_id_kd_nilai,
				d.tingkat as tingkat,
				b.kelas as kelas
			FROM kd_nilai as a
			INNER JOIN rencana_penilaian as c ON a.rencana_penilaian_id = c.id
			INNER JOIN rombongan_belajar as d ON c.rombongan_belajar_id = d.id
			INNER JOIN ref_kompetensi_dasar as b ON a.id_kompetensi = b.id_kompetensi AND b.mata_pelajaran_id = c.mata_pelajaran_id AND b.kelas = d.tingkat			
			LIMIT 0,10
		");
		$result = $query->row();
		if($result){
			if($result->id_ref_kd != $result->ref_kd_id_kd_nilai){
				$this->db->query("
					UPDATE kd_nilai as a 
					INNER JOIN rencana_penilaian as c ON a.rencana_penilaian_id = c.id
					INNER JOIN rombongan_belajar as d ON c.rombongan_belajar_id = d.id
					INNER JOIN ref_kompetensi_dasar as b ON a.id_kompetensi = b.id_kompetensi AND b.mata_pelajaran_id = c.mata_pelajaran_id AND b.kelas = d.tingkat
					SET a.kd_id = b.id;
				");
			}
		}
		redirect('test/lanjut');
	}
	public function lanjut(){
		$this->db->select('a.kompetensi_dasar_id as kompetensi_dasar_id_nilai, b.kd_id as kompetensi_dasar_id_kd_nilai');
		$this->db->from('nilai as a');
		$this->db->join('kd_nilai as b', 'a.kd_nilai_id = b.id');
		$this->db->limit(10);
		$query = $this->db->get();
		$result = $query->row();
		if($result){
			if($result->kompetensi_dasar_id_nilai != $result->kompetensi_dasar_id_kd_nilai){
				$this->db->query("
					UPDATE nilai as a 
					INNER JOIN kd_nilai as b ON a.kd_nilai_id = b.id
					SET a.kompetensi_dasar_id = b.kd_id;
				");
			}
		}
		redirect('test/satu');
	}
	public function satu(){
		update_pembelajaran();
		redirect('test/dua');
	}
	public function dua(){
		update_nilai();
		redirect('test/tiga');
	}
	public function tiga(){
		update_rencana();
		redirect('test/empat');
	}
	public function empat(){
		update_remedial();
		redirect('test/lima');
	}
	public function lima(){
		update_deskripsi_mata_pelajaran();
		redirect('test/enam');
	}
	public function enam(){
		update_import_rencana();
		redirect('test/tujuh');
	}
	public function tujuh(){
		update_sikap();
		redirect('');
	}
}
/* End of file test.php */
/* Location: ./application/controllers/welcome.php */