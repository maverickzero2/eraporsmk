<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Nilai_model extends MY_Model{
	public $_table = 'nilai';
	public $primary_key = 'nilai_id';
	public $belongs_to = array(
		'rencana_penilaian' => array('model' => 'rencana_penilaian_model', 'primary_key' => 'rencana_penilaian_id'),
		'kompetensi_dasar' => array('model' => 'kompetensi_dasar_model', 'primary_key' => 'kompetensi_dasar_id')
	);//1 ke 1
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}