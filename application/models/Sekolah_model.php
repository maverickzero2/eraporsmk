<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Sekolah_model extends MY_Model{
	public $_table = 'ref_sekolah';
	public $primary_key = 'sekolah_id';
	//public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
		if(is_array($data)){
        	$data['last_sync'] = date('Y-m-d H:i:s');
		} else {
			$data->last_sync = date('Y-m-d H:i:s');
		}
        return $data;
    }
}