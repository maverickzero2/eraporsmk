<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_kompetensi_dasar_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'id_kompetensi' => array(
				'type' => 'VARCHAR',
				'constraint' => 11
			),
      		'aspek' => array(
				'type' => 'VARCHAR',
				'constraint' => 11
			),
      		'mata_pelajaran_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
      		'kelas' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
      		'id_kompetensi_nas' => array(
				'type' => 'VARCHAR',
				'constraint' => 11,
				'null'	=> true
			),
      		'kompetensi_dasar' => array(
				'type' => 'TEXT'
			),
      		'kompetensi_dasar_alias' => array(
				'type' => 'TEXT',
				'null'	=> true
			),
      		'user_id' => array(
				'type' => 'uuid',
				'null'	=> true,
			),
      		'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' 	=> 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 	=> 'timestamp(0) without time zone',
				'default' => '2018-01-01 00:00:00'
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id',TRUE);
		$this->dbforge->create_table('ref_kompetensi_dasar',TRUE);
		$kompetensi_dasar = array(1, 10037);
		foreach($kompetensi_dasar as $key => $dasar){
			$this->db->select('*');
			$this->db->from('ref_kompetensi_dasar');
			$this->db->where('id',$dasar);
			$query = $this->db->get();
			$result = $query->row();
			if(!$result){
				include_once APPPATH."/migrations/referensi/ref_kompetensi_dasar($key).php";
				$this->db->insert_batch('ref_kompetensi_dasar', $ref_kompetensi_dasar);
			}
			sleep(1);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_kompetensi_dasar', TRUE);
	}
}