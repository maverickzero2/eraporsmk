<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_prakerin_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){ 
		$fields = array(
			'prakerin_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id'	=> array(
				'type' => 'uuid',
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'mitra_prakerin' => array(
				'type' => 'VARCHAR',
				'null'	=> true
			),
			'lokasi_prakerin' => array(
				'type' => 'VARCHAR',
				'null'	=> true
			),
			'lama_prakerin' => array(
				'type' => 'VARCHAR',
				'null'	=> true
			),
			'keterangan_prakerin' => array(
				'type' => 'TEXT',
				'null'	=> true
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('prakerin_id', TRUE);
		$this->dbforge->create_table('prakerin',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('prakerin', TRUE);
	}
}