<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_rencana_penilaian_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'rencana_penilaian_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id'	=> array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'mata_pelajaran_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id' => array(
				'type' => 'uuid',
			),
			'kompetensi_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'nama_penilaian' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'metode_id'	=> array(
				'type' => 'uuid',
			),
			'bobot'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'keterangan' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('rencana_penilaian_id', TRUE);
		$this->dbforge->create_table('rencana_penilaian',TRUE);
	}
	public function down(){
		$this->dbforge->drop_table('rencana_penilaian', TRUE);
	}
}