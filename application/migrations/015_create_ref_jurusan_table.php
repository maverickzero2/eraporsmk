<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_jurusan_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$sql = "CREATE TABLE ref_jurusan (
					jurusan_id character varying(25) NOT NULL,
					nama_jurusan character varying(100) NOT NULL,
					untuk_sma numeric(1,0) NOT NULL,
					untuk_smk numeric(1,0) NOT NULL,
					untuk_pt numeric(1,0) NOT NULL,
					untuk_slb numeric(1,0) NOT NULL,
					untuk_smklb numeric(1,0) NOT NULL,
					jenjang_pendidikan_id numeric(2,0),
					jurusan_induk character varying(25),
					level_bidang_id character varying(5) NOT NULL,
					created_at timestamp(0) without time zone NOT NULL,
					updated_at timestamp(0) without time zone NOT NULL,
					deleted_at timestamp(0) without time zone,
					last_sync timestamp(0) without time zone NOT NULL,
					CONSTRAINT jurusan_pkey PRIMARY KEY (jurusan_id),
					CONSTRAINT jurusan_jurusan_induk_fkey FOREIGN KEY (jurusan_induk)
						REFERENCES ref_jurusan (jurusan_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION
				);";
		$this->db->query($sql);
		$this->db->select('*');
		$this->db->from('ref_jurusan');
		$this->db->where('jurusan_id','00');
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			include_once APPPATH."/migrations/referensi/ref_jurusan.php";
			$this->db->insert_batch('ref_jurusan', $ref_jurusan);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_jurusan', TRUE);
	}
}