<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_teknik_penilaian_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'teknik_penilaian_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'kompetensi_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'nama'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('teknik_penilaian_id', TRUE);
		$this->dbforge->create_table('teknik_penilaian',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('teknik_penilaian', TRUE);
	}
}