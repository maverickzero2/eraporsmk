<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_kompetensi_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
      		'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id',TRUE);
		$this->dbforge->create_table('ref_kompetensi',TRUE); 
		$ref_kompetensi = array(
				array('id' => '1','nama' => 'Pengetahuan','created_at' => '2017-09-19 01:15:21','updated_at' => '2017-09-19 01:15:21'),
				array('id' => '2','nama' => 'Keterampilan','created_at' => '2017-09-19 01:15:21','updated_at' => '2017-09-19 01:15:21'),
			);
			$this->db->insert_batch('ref_kompetensi', $ref_kompetensi);
	}
	public function down(){
		$this->dbforge->drop_table('ref_kompetensi', TRUE);
	}
}