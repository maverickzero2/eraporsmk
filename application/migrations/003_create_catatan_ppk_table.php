<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_catatan_ppk_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'catatan_ppk_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'siswa_id'		=> array(
				'type' => 'uuid',
			),
			'capaian' => array(
				'type' => 'TEXT'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('catatan_ppk_id', TRUE);
		$this->dbforge->create_table('catatan_ppk',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('catatan_ppk', TRUE);
	}
}