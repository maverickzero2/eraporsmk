<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_settings_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'site_title' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'version' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'periode' => array(
				'type' => 'TEXT',
			),
			'sinkronisasi'	=> array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'zona'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 1
			),
			'tanggal_rapor' => array(
				'type' => 'DATE',
			),
			'import'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 1
			),
			'desc_mapel'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 1
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('settings',TRUE);
		$tahun = date('Y');
		$bulan = date('m');
		$smt1 = array(7, 8, 9, 10, 11, 12);
		$smt2 = array(1, 2, 3, 4, 5, 6);
		if (in_array($bulan, $smt1)) {
			$periode	= $tahun.'/'.($tahun+1). ' | Semester Ganjil';
		} else {
			$periode	= ($tahun-1).'/'.$tahun. ' | Semester Genap';
		}
		$setting_data = array(
			'id'			=> 1,
			'site_title'	=> 'eRapor SMK',
			'version'		=> '4.0.2',
			'periode'		=> $periode,
			'sinkronisasi'	=> 1,
			'zona'			=> 1,
			'tanggal_rapor'	=> date('Y-m-d'),
			'import'		=> 0,
			'desc_mapel'	=> 0,
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s'),
			'last_sync'		=> date('Y-m-d H:i:s'),
		);
		$this->db->select('*');
		$this->db->from('settings');
		$this->db->where('id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			$this->db->insert('settings', $setting_data);
		}
	}
	public function down(){
		$this->dbforge->drop_table('settings', TRUE);
	}
}