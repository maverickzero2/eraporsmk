<?php
/**
 * CodeIgniter Migrate
 *
 * @author  Natan Felles <natanfelles@gmail.com>
 * @link    http://github.com/natanfelles/codeigniter-migrate
 */
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @var array $assets             Assets links
 * @var bool  $migration_disabled Migration status
 * @var array $migrations         Migration files
 * @var int   $current_version    Current migration version
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Migrate</title>
    <link rel="stylesheet" href="<?= $assets['bootstrap_css'] ?>">
    <style>
        body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif; text-align:center;}

        .btn-migrate {
            width: 100%;
        }
		.loader {
margin:50px auto 20px auto;
  border: 46px solid #f3f3f3;
  border-radius: 50%;
  border-top: 46px solid blue;
  border-right: 46px solid green;
  border-bottom: 46px solid red;
  border-left: 46px solid pink;
  width: 200px;
  height: 200px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
    </style>
</head>
<body>
<!--div class="loader"></div>
<p>Mohon menunggu, sedang membuat table <span class="table"></span> database</p>
<p>Laman ini akan otomatis beralih ke laman register </p-->
<div class="container">
    <h1 class="text-center">
        <i class="glyphicon glyphicon-fire"></i> eRaporSMK Migrate<br>
        <small>An easy way to manage database migrations</small>
    </h1>
	<?php if (isset($migration_disabled)) : ?>
        <div class="alert alert-info">Migration is disabled.</div>
	<?php else : ?>
        <div class="row">
            <div class="col-md-9">
                <div id="msg-migrate">
                    <div class="msg">
                        <div class="alert alert-info">
                            <strong>Info</strong><br> The current migration version is
                            <strong><?= $current_version ?></strong>. 
							<strong id="get_migrate"></strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="well">
                    <div class="btn-group btn-group-justified" role="group">
                        <div class="btn-group" role="group">
                            <button class="btn btn-danger btn-migrate" id="reset_migrate" data-version="0"
                                    autocomplete="off">
                                Reset
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th class="text-center">Order</th>
                <th>Version</th>
                <th>File</th>
				<th>Table</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody id="migrations">
			<?php if (empty($migrations)) : ?>
                <tr>
                    <td colspan="5">No migrations.</td>
                </tr>
			<?php else : ?>
				<?php $i=1; foreach ($migrations as $migration) :?>
                    <tr<?= $migration['version'] != $current_version ? '' : ' class="success"' ?>>
                        <!--th class="text-center"><?= isset($order) ? --$order : $order = count($migrations) ?></th-->
						<?php
						//$table = substr($migration['file'], -1, 10);
						$table = str_replace('_create_', '', $migration['file']);
						$table = preg_replace('/[0-9]+/', '',$table);
						$table = str_replace('migrations/', '', $table);
						$table = str_replace('_table.php', '', $table);
						if (!$this->db->table_exists($table)){ ?>
						<th class="text-center"><?= $i; ?></th>
                        <td><?= $migration['version'] ?></td>
                        <td><?= $migration['file'] ?></td>
						<td><?= $table ?></td>
                        <td>
                            <button data-version="<?= $migration['version'] ?>" class="btn btn-sm btn-primary btn-migrate" autocomplete="off" id="migrate" data-table="<?= $table ?>">Migrate</button>
                        </td>
						<?php } ?>
                    </tr>
				<?php $i++;endforeach ?>
			<?php endif ?>
            </tbody>
        </table>
	<?php endif ?>
</div>
<script src="<?= $assets['jquery'] ?>"></script>
<script src="<?= $assets['bootstrap_js'] ?>"></script>
<script>
    $(document).ready(function () {
		var btn = $('#migrate');
		//console.log(btn);
		var d = {
            name: 'version',
        	value: btn.data('version')
        };
		$.ajax("<?= site_url('migrate/token') ?>", {
			cache: false,
			error: function () {
				msg('#msg-migrate', 'danger', {content: 'CSRF Token could not be get.'});
			}
		}).done(function (t) {
			//console.log(t);
			$('.table').html('<strong>'+$('#migrate').data('table')+'</strong>');
			d = $.merge($.makeArray(d), $.makeArray(t));
			console.log(d);
			$.post("<?= site_url('migrate/post') ?>", d, function (r) {
				console.log(r);
				msg('#msg-migrate', r.type, r);
				if (r.type === 'success') {
					$('#migrations').children('tr').removeClass('success');
					btn.parent().parent().addClass('success');
					window.setTimeout(function() {
						window.location = '<?php echo site_url(); ?>';
					}, 5);
				}
			btn.button('reset');
			}, 'json').fail(function () {
				msg('#msg-migrate', 'danger', {content: 'Something is wrong.'});
			});
		});
		var btn_migrate = $('#reset_migrate');
        btn_migrate.prepend('<i class="glyphicon glyphicon-refresh"></i> ');
        btn_migrate.click(function () {
            var btn = $(this);
            btn.button('loading');
            //console.log(btn.data('version'));
            var d = {
                name: 'version',
                value: btn.data('version')
            };
            $.when($.ajax("<?= site_url('migrate/token') ?>", {
                cache: false,
                error: function () {
                    msg('#msg-migrate', 'danger', {content: 'CSRF Token could not be get.'});
                }
            })).done(function (t) {
                console.log(t);
                d = $.merge($.makeArray(d), $.makeArray(t));
                console.log(d);
                $.post("<?= site_url('migrate/post') ?>", d, function (r) {
                    console.log(r);
                    msg('#msg-migrate', r.type, r);
                    if (r.type === 'success') {
                        $('#migrations').children('tr').removeClass('success');
                        btn.parent().parent().addClass('success');
						window.setTimeout(function() {
							window.location = '<?php echo site_url(); ?>';
						}, 5);
                    }
                    btn.button('reset');
                }, 'json').fail(function () {
                    msg('#msg-migrate', 'danger', {content: 'Something is wrong.'});
                });
            });
            return false;
        });
    });

    function msg(parent, type, r) {
        var h = '';
        if (r.header) {
            h += '<strong>' + r.header + '</strong><br>';
        }
        // If Response Content is an Object we will list it
        if (typeof r.content === 'object') {
            var o = '<ul>';
            $.each(r.content, function (k, v) {
                o += '<li>' + v + '</li>';
            });
            o += '</ul>';
            h += o;
        } else {
            h += r.content;
        }
        $(parent).children('.msg')
            .removeClass()
            .addClass('msg alert alert-' + type)
            .html(h + '<span class="pull-right">' + new Date().toLocaleTimeString() +'</span>');
    }
</script>
</body>
</html>
