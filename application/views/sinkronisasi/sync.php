<div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-success">
	<div class="box-header">
		<i class="fa fa-bank"></i>
		<h3 class="box-title">Identitas Sekolah</h3>
	</div>
	<div class="box-body">
		<table class="table">
			<tr>
				<th width="40%">NPSN Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? $sekolah->npsn : '-'; ?></th>
			</tr>
			<tr>
				<th width="40%">Nama Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? $sekolah->nama : '-'; ?></th>
			</tr>
			<tr>
				<th width="40%">Alamat Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? $sekolah->alamat : '-'; ?></th>
			</tr>
			<tr>
				<th width="40%">Kepala Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? get_nama_guru($sekolah->guru_id) : '-'; ?></th>
			</tr>
		</table> 
    </div><!-- /.box-body -->
</div><!-- /.box -->
<?php
$max_input_vars = ini_get('max_input_vars');
$last_sync = date('Y-m-d H:i:s');
$settings = $this->settings->get(1);
if(!isset($settings->last_sync)){
	if ($this->db->field_exists('last_sync', 'settings')){
		$this->settings->update(1, array('desc_mapel' => 0));
		redirect('admin/sinkronisasi/sync');
	}
} else {
	$last_sync = $settings->last_sync;
}
$status = checkdnsrr('php.net');
$connect = ($status) ? 'bg-green' : 'bg-red';
$text = ($status) ? 'TERHUBUNG' : 'TIDAK TERHUBUNG';
$tombol = ($status) ? '' : 'disabled';
$table_sync = array(
	1	=> 'absen',
	2	=> 'anggota_rombel',
	3	=> 'catatan_ppk',
	4	=> 'catatan_wali',
	5	=> 'deskripsi_mata_pelajaran',
	6	=> 'deskripsi_sikap',
	7	=> 'ekstrakurikuler',
	8	=> 'guru_terdaftar',
	9	=> 'jurusan_sp',
	10	=> 'kd_nilai',
	11	=> 'nilai',
	11	=> 'nilai_ekstrakurikuler',
	12	=> 'nilai_sikap',
	13	=> 'pembelajaran',
	14	=> 'prakerin',
	15	=> 'prestasi',
	16	=> 'ref_guru',
	17	=> 'ref_sekolah',
	18	=> 'ref_sikap',
	19	=> 'ref_siswa',
	20	=> 'remedial',
	21	=> 'rencana_penilaian',
	22	=> 'rombongan_belajar',
	23	=> 'teknik_penilaian',
);
?>
<div class="box box-warning">
	<div class="box-header">
		<i class="fa fa-signal"></i>
		<h3 class="box-title">STATUS KONEKSI : <span class="label <?php echo $connect; ?>"><?php echo $text; ?></span></h3>
	</div>
	<div class="box-body text-center">
		<p>Pengiriman data dilakukan terakhir <strong><?php echo TanggalIndo($last_sync); ?></strong></p>
		<p><button type="button" class="btn btn-success btn-lrg ajax <?php echo $tombol; ?>" title="KIRIM DATA" data-loading-text="<i class='fa fa-spinner fa-spin '></i> &nbsp; SEDANG PROSES SINKRONISASI">
            <i class="fa fa-refresh"></i>&nbsp; KIRIM DATA
          </button></p>
		<div class="color-palette-set">
			<div id="status" class="bg-black-active color-palette" style="padding:5px; display:none;"></div>
		</div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<div class="box box-primary">
	<div class="box-header">
		<i class="fa fa-reorder"></i>
		<h3 class="box-title">DATA YANG MENGALAMI PERUBAHAN</h3>
	</div>
	<div class="box-body">
		<table class="table table-bordered">
			<tr>
				<th style="width: 10px">No.</th>
				<th>Table</th>
				<!--th style="width:100px;" class="text-center">Status</th-->
				<th style="width: 100px" class="text-center">Jumlah Data</th>
			</tr>
			<?php
			$i=1;
			$total = 0;
			foreach($table_sync as $sync){
				$this->db->select('*');
				$this->db->from($sync);
				//$this->db->where("last_sync >= '$last_sync'");
				$this->db->where('last_sync >=', $last_sync);
				$result = $this->db->count_all_results();
				//$result = $query->count_all_results();
				if($max_input_vars > $result){
					$status = '<label class="label bg-green">OK</label>';
				} else {
					$status = '<label class="label bg-red">NO</label>';
				}
				if($result){
					$total += $result;
			?>
			<tr>
				<td class="text-center"><?php echo $i; ?></td>
				<td><?php echo $sync; ?></td>
				<!--td class="text-center"><?php echo $status; ?></td-->
				<td class="text-right"><?php echo number_format($result,0, '', '.'); ?></td>
			</tr>
			<?php $i++;}
			} ?>
			<tr>
				<td colspan="2" class="text-right"><strong>T O T A L</strong></td>
				<td class="text-right"><strong><?php echo number_format($total,0, '', '.'); ?></strong></td>
			</tr>
		</table>    
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>
<script>
var timer;
$('.ajax').click(function(){
	$('#status').html('Mengirim data');
	//timer = window.setInterval(refreshProgress, 1000);
	//$('#spinner').remove();
	$('#status').show();
	var btn = $(this);
	btn.button('loading');
	$.ajax({
		url: '<?php echo site_url('admin/proses_sync'); ?>',
		//type: 'post',
		//data: form_data,
		success: function(response){
			//$('#status').hide();
			btn.button('reset');
			swal({title:"Sukses", text:"Sinkronisasi Selesai", type:"success"}).then(function() {
				window.location.replace('<?php echo site_url('admin/sinkronisasi/sync'); ?>');
		}).done();
		}
	});
});
function refreshProgress() {
	$.ajax({
        url: "<?php echo site_url('admin/proses_sync/test'); ?>",
        success:function(data){
			$('#status').html(data);
			//$('#status').html('5% [Mengirim data siswa]');
		}
	});
}
</script>