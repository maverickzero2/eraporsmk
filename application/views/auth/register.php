<div class="login-logo">
	<img src="<?php echo base_url(); ?>assets/img/logo.png" alt="logo" class="text-center" width="100" />
	<!--a href="<?php echo site_url(); ?>"><b>e-Rapor SMK</b></a-->
</div>
<div class="login-box-body">
	<p class="login-box-msg"><a href="<?php echo site_url(); ?>"><b>e-Rapor SMK</b></a></p>
    <?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
    <?php echo isset($error) ? error_msg($error) : ''; ?>
    <?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
    <form action="<?php echo site_url('register'); ?>" method="post">
        <div class="form-group has-feedback">
			<input type="text" name="npsn" class="form-control" placeholder="NPSN"/>
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="text" name="email" class="form-control" placeholder="Email Dapodik"/>
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password" class="form-control" placeholder="Password Dapodik"/>
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password_confirm" class="form-control" placeholder="Confirm Password"/>
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
		</div>
    </form>
</div>