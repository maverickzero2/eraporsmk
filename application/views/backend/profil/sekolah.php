<?php
//$readonly = '';
//$disabled = '';
$loggeduser = $this->ion_auth->user()->row();
$ajaran = get_ta();
$data_sekolah = $this->sekolah->get($loggeduser->sekolah_id);
$sekolah_id = $loggeduser->sekolah_id;
//if($loggeduser->siswa_id || $loggeduser->guru_id){
	$readonly = 'readonly="true"';
	$disabled = 'disabled';
//}
?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
		<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
        <?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
        <div class="box box-primary">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-university"></i> <?php echo $page_title; ?></a></li>
					<li><a href="#tab_2" data-toggle="tab"><i class="fa fa-anchor"></i> Kompetensi Keahlian Dilayani</a></li>
					<li><a href="#tab_3" data-toggle="tab"><i class="fa fa-camera"></i> Upload Logo</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-body">
						<table class="table table-bordered table-striped">
							<tr>
								<td style="width: 30%;">Nama Sekolah</td>
								<td style="width: 65%"><?php echo $sekolah->nama; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">NPSN / NSS</td>
								<td style="width: 65%"><?php echo $sekolah->npsn; ?> / <?php echo $sekolah->nss; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Alamat</td>
								<td style="width: 65%"><?php echo $sekolah->alamat; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Kode Pos</td>
								<td style="width: 65%"><?php echo $sekolah->kode_pos; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Telepon</td>
								<td style="width: 65%"><?php echo $sekolah->no_telp; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Kelurahan</td>
								<td style="width: 65%"><?php echo $sekolah->desa_kelurahan; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Lintang</td>
								<td style="width: 65%"><?php echo $sekolah->lintang; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Bujur</td>
								<td style="width: 65%"><?php echo $sekolah->bujur; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Kecamatan</td>
								<td style="width: 65%"><?php echo $sekolah->kecamatan; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Kota/Kabupaten</td>
								<td style="width: 65%"><?php echo $sekolah->kabupaten; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Provinsi</td>
								<td style="width: 65%"><?php echo $sekolah->provinsi; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Email</td>
								<td style="width: 65%"><?php echo $sekolah->email; ?></td>
							</tr>
							<tr>
								<td style="width: 30%;">Website</td>
								<td style="width: 65%"><?php echo $sekolah->website; ?></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="tab_2">
					<div class="box-body">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center">No.</th>
									<th>Bidang Keahlian</th>
									<th>Program Keahlian</th>
									<th>Kompetensi Keahlian</th>
									<th>Nama Jurusan Satuan Pendidikan</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$jurusan_sp = $this->jurusan_sp->find_all("sekolah_id = '$loggeduser->sekolah_id'");
								$i=1;
								foreach($jurusan_sp as $jurusan){
									$get_kompetensi = $this->db->get_where('ref_jurusan', array('jurusan_id' => $jurusan->jurusan_id));
									$get_kompetensi = $get_kompetensi->row();
									$get_program = $this->db->get_where('ref_jurusan', array('jurusan_id' => $get_kompetensi->jurusan_induk));
									$get_program = $get_program->row();
									$get_bidang = $this->db->get_where('ref_jurusan', array('jurusan_id' => $get_program->jurusan_induk));
									$get_bidang = $get_bidang->row();
									$bidang_keahlian = ($get_bidang) && ($get_bidang->level_bidang_id == 10) ? $get_bidang->nama_jurusan : $get_program->nama_jurusan;
									$program_keahlian = ($get_program) && ($get_program->level_bidang_id == 11) ? $get_program->nama_jurusan : $get_kompetensi->nama_jurusan;
									$kompetensi_keahlian = ($get_kompetensi) && ($get_kompetensi->level_bidang_id == 12) ? $get_kompetensi->nama_jurusan : '-';
								?>
								<tr>
									<td class="text-center"><?=$i;?></td>
									<td><?=$bidang_keahlian;?></td>
									<td><?=$program_keahlian;?></td>
									<td><?=$kompetensi_keahlian;?></td>
									<td><?=$jurusan->nama_jurusan_sp;?></td>
								</tr>
								<?php $i++;} ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane" id="tab_3">
					<form role="form" method="POST" action="<?php echo site_url('admin/profil/update_logo'); ?>" enctype="multipart/form-data">
						<div class="box-body">
							<div class="form-group col-xs-6">
								 <p><img src="<?php echo ($sekolah->logo_sekolah) ? base_url().PROFILEPHOTOSTHUMBS.$sekolah->logo_sekolah: base_url().'assets/img/300.gif'; ?>" class="img-responsive thumbnail center-block" alt="Responsive image"/></p>
							</div>
							<div class="form-group col-xs-6">
								<div class="form-group">
									<label>Ganti Logo Sekolah</label>
									<input type="file" name="profilephoto" />
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary">Simpan</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div><!-- /.box -->
		</div><!--/.col (left) -->
	</div>
</div>