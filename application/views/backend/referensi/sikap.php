<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
        <?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
				<th style="width: 10px;" class="text-center">No.</th>
                <th style="width: 100%">Nama Sikap</th>
            </tr>
            </thead>
			<tbody>
				<?php
				$all_sikap = $this->sikap->get_all(); 
				$i=1;
				foreach($all_sikap as $sikap){
				?>
				<tr>
					<td class="text-center"><?=$i;?></td>
					<td><?php echo $sikap->butir_sikap; ?></td>
				</tr>
				<?php $i++; } ?>
			</tbody>      
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>