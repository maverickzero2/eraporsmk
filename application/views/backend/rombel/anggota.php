<div class="row">
	<div class="col-xs-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width: 3%" class="text-center">No.</th>
					<th style="width: 30%" class="text-center">Nama</th>
					<th style="width: 10%" class="text-center">NISN</th>
					<th style="width: 3%" class="text-center">L/P</th>
					<th style="width: 25%" class="text-center">Tempat, Tanggal Lahir</th>
					<th style="width: 10%" class="text-center">Agama</th>
					<th style="width: 10%" class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if($anggota){
					$i=1;
					foreach($anggota['data'] as $siswa){
						if($siswa){
						//test($siswa);
						$date = date_create($siswa->tanggal_lahir);
				?>
					<tr>
						<td class="text-center"><?php echo $i; ?></td>
						<td><?php echo $siswa->nama; ?></td>
						<td class="text-center"><?php echo $siswa->nisn; ?></td>
						<td><?php echo $siswa->jenis_kelamin; ?></td>
						<td><?php echo $siswa->tempat_lahir.', '.TanggalIndo(date_format($date,'Y-m-d')); ?></td>
						<td class="text-center"><?php echo get_agama($siswa->agama_id); ?></td>
						<td class="text-center"><a class="btn btn-sm btn-danger confirm" href="<?php echo site_url('admin/rombel/keluarkan/'.$siswa->siswa_id); ?>">Keluarkan</a></td>
					</tr>
				<?php
					$i++;
					}
					}
				} else {
				?>
					<tr>
						<td colspan="6">Anggota rombel tidak ditemukan</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<script>
	$('a.confirm').bind('click',function(e) {
		var ini = $(this).parents('tr');
		e.preventDefault();
		var url = $(this).attr('href');
		swal({
			title: "Anda Yakin?",
			text: "Tindakan ini tidak bisa dikembalikan!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Keluarkan!",
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve) {
					$.get(url)
					.done(function(response) {
						var data = $.parseJSON(response);
						swal({title:data.title, html:data.text, type:data.type}).then(function() {
							ini.remove();
							var id_datatable = $('body').find('#datatable');
							if(id_datatable.length>0){
								$('#datatable').dataTable().fnReloadAjax();
							}
						}).done();
					})
				})
			}
		}).done();
	});
</script>