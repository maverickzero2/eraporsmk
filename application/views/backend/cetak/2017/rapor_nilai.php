<?php
$uri = $this->uri->segment_array();
if(isset($uri[3])){
    if($uri[3] == 'review_rapor'){
        $border = '';
        $class = 'table table-bordered';
    } else {
        $border = 'border="1"';
        $class = 'table';
    }
}
$data['s'] = $this->siswa->get($siswa_id);
$sekolah = $this->sekolah->get($sekolah_id);
$setting = $this->settings->get(1);
$data['rombel'] = $this->rombongan_belajar->get($rombel_id);
$ajaran = $this->semester->get($ajaran_id);
$data['mapel_a'] = $this->pembelajaran->with('mata_pelajaran')->find_all("semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 6) OR semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 6)", '*','no_urut ASC');
foreach($data['mapel_a'] as $mapela){
	$mapel_a_id[] = $mapela->mata_pelajaran_id;
}
if(isset($mapel_a_id)){
	$mapel_agama = array(100011070, 100012050, 100013010, 100014140, 100015010, 100016010);
	$data['mapel_a'] = filter_agama_mapel($ajaran_id,$mapel_agama, $mapel_a_id,$data['s']->agama_id);
}
//test($mapel_a);
$data['mapel_b'] = $this->pembelajaran->with('mata_pelajaran')->find_all("semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 7) OR semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 7)", '*','no_urut ASC');
$data['mapel_c1'] = $this->pembelajaran->with('mata_pelajaran')->find_all("semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 8) OR semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 8)", '*','no_urut ASC');
$data['mapel_c2'] = $this->pembelajaran->with('mata_pelajaran')->find_all("semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 9) OR semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 9)", '*','no_urut ASC');
$data['mapel_c3'] = $this->pembelajaran->with('mata_pelajaran')->find_all("semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 10) OR semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 10)", '*','no_urut ASC');
$data['mapel_tambahan'] = $this->pembelajaran->with('mata_pelajaran')->find_all("semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 99) OR semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id' AND guru_pengajar_id IS NOT NULL AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM mata_pelajaran_kurikulum WHERE kelompok_id = 99)", '*','no_urut ASC');
$data['nama_kelompok_a'] = $this->kelompok->get(6);
$data['nama_kelompok_b'] = $this->kelompok->get(7);
$data['nama_kelompok_c1'] = $this->kelompok->get(8);
$data['nama_kelompok_c2'] = $this->kelompok->get(9);
$data['nama_kelompok_c3'] = $this->kelompok->get(10);
?>
<div class="strong">C.&nbsp;&nbsp;Pengetahuan dan Keterampilan</div>
<table <?php echo $border; ?> class="<?php echo $class; ?>">
    <thead>
  <tr>
    <th style="vertical-align:middle;width: 2px;" align="center" rowspan="2">No</th>
    <th style="vertical-align:middle;width: 200px;" rowspan="2" align="center" class="text-center">Mata Pelajaran</th>
    <th colspan="4" align="center" class="text-center">Pengetahuan</th>
    <th colspan="4" align="center" class="text-center">Keterampilan</th>
  </tr>
  <tr>
    <th align="center" style="width:10px;" class="text-center">KKM</th>
    <th align="center" style="width:10px;" class="text-center">Angka</th>
    <th align="center" style="width:10px;" class="text-center">Predikat</th>
    <th align="center" style="width:150px;" class="text-center">Deskripsi</th>
    <th align="center" style="width:10px;" class="text-center">KKM</th>
    <th align="center" style="width:10px;" class="text-center">Angka</th>
    <th align="center" style="width:10px;" class="text-center">Predikat</th>
    <th align="center" style="width:150px;" class="text-center">Deskripsi</th>
  </tr>
    </thead>
    <tbody>
		<?php
		$count_a = count($data['mapel_a']);
		$count_b = count($data['mapel_b']);
		$count_c1 = 0;
		$count_c2 = 0;
		$count_c3 = 0;
		$this->load->view('backend/cetak/2017/a',$data);
		$data['i'] = $count_a + 1;
		$this->load->view('backend/cetak/2017/b',$data);
		//if($data['rombel']->tingkat != 12){
		$count_c1 = count($data['mapel_c1']);
		$count_c2 = count($data['mapel_c2']);
		$data['i'] = $count_a + $count_b + 1;
		$this->load->view('backend/cetak/2017/c1',$data);
		$data['i'] = $count_a + $count_b + $count_c1 + 1;
		$this->load->view('backend/cetak/2017/c2',$data);
		//}
		//if($data['rombel']->tingkat != 10){
		$count_c3 = count($data['mapel_c3']);
		$data['i'] = $count_a + $count_b + $count_c1 + $count_c2 + 1;
		$this->load->view('backend/cetak/2017/c3',$data);
		//}
		$data['i'] = $count_a + $count_b + $count_c1 + $count_c2 + $count_c3 + 1;
		$this->load->view('backend/cetak/2017/m',$data);
		?>
	</tbody>
</table>