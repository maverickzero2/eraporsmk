<?php
if($mapel_a){
?>
<tr>
	<td colspan="10" class="strong"><?php echo $nama_kelompok_a->nama_kelompok; ?></td>
</tr>
<?php
$i=1;
foreach($mapel_a as $mapela){
	$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapela, $siswa_id);
	$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapela, $siswa_id);
	$deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapela, $siswa_id,1);
	$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapela, $siswa_id,2);
?>
<tr>
	<td align="center" valign="top"><?php echo $i; ?></td>
	<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapela); ?></td>
	<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
	<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
	<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_pengetahuan_value); ?></td>
	<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
	<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
	<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
	<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_keterampilan_value); ?></td>
	<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
</tr>
<?php
	$i++; 
}
}
?>